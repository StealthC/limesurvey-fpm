# LimeSurvey running on a php:7-fpm-alpine

https://hub.docker.com/r/stealthc/limesurvey-fpm

## Usage example (Nginx + MariaDB)

docker-compose.yml:

```
version: '3'
services:
  web:
    image: nginx:latest
    ports:
        - "8080:80"
    depends_on:
      - limesurvey
    volumes:
      - ./nginx:/etc/nginx/conf.d
      - ./logs:/var/log/nginx
      - ./www-data:/var/www/html
    links:
      - limesurvey
    restart: always
  limesurvey:
    image: stealthc/limesurvey-fpm
    volumes:
      - ./www-data:/var/www/html
    restart: always
    depends_on:
      - db
    links:
      - db
  db:
    image: mariadb
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: limesurvey
```


default.conf:

```
server {
    listen 80;
    server_name _;

    root /var/www/html;
    index index.php;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass limesurvey:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}
```